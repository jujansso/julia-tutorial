using Plots

x = 1:0.01:10*π
y = sin.(x)

plot(x, y, label="sin(x)")
plot!(xlab="x", ylab="f(x)")
#! means modification of current plot
y2=sin.(x).^2
plot!(x, y2, label="sin(x)^2", color=:red, line=:dash)

xaxis!(:log10)
plot!(legend=:bottomleft)

plotly()
x=1:0.1:3*π
y=1:0.1:3*π

xx = reshape([xi for xi in x for yj in y],  length(y), length(x))
yy = reshape([yj for xi in x for yj in y],  length(y), length(x))
zz = sin.(xx).*cos.(yy)
plot3d(xx,yy,zz, label=:none, st = :surface)
plot!(xlab="x", ylab="y", zlab="sin(x)*cos(y)")

x=0:0.1:2*π
y=sin.(x).^2

plot(x, y, label="sin(x)^2")


plot(x, y, label=L"$\sin(x)^2$")

#14 data storage

using JLD

x = collect(-3:0.1:3)
y = collect(-3:0.1:3)

xx = reshape([xi for xi in x for yj in y], length(y), length(x))
yy = reshape([yj for xi in x for yj in y], length(y), length(x))
                                
z = sin.(xx .+ yy.^2)

data_dict = Dict("x" => x, "y" => y, "z" => z)

save("data_dict.jld", data_dict)

data_dict2 = load("data_dict.jld")

x2 = data_dict2["x"]
y2 = data_dict2["y"]
z2 = data_dict2["z"]

using Plots
plotly()
#another package has to be installed??
plot(x2, y2, z2, st = :surface, color = :ice)
#Plotting not working, just showing up blank, something to do with remote environment?


using JLD
struct Person
    height::Float64
    weight::Float64
end

bob = Person(1.84, 74)

dict_new = Dict("bob" => bob)
save("bob.jld", dict_new)

bob2 = load("bob.jld")

#Parallell computing
using SpecialFunctions

x = range(0,100, length=10000)

results = zeros(length(x))

results .= besselj1.(x)

using Pkg
Pkg.add("BenchmarkTools")
using BenchmarkTools

Threads.@threads for i in 1:length(x)
    results[i] = besselj1(x[i])
end

#Type stability is important so use @code_warntype macro!
#@profile for checking slowness
#again remember column before row!
#positional arguments better than keywords
#tests can also be written!

"Techy"*"Tok"

#multiple dispatch is like override in java, for different types. Could be useful.

