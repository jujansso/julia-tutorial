println(PROGRAM_FILE); for x in ARGS; println(x); end
2+2

## this is a code cell
println("Hello")
println("This is TechyTok!")
##
## this is another code cell
println(42)
##
s = convert(Float64,2)

function plus_two(x)
    #perform some operations
    return x + 2
end

# Three ways to write functions
plus_t(x) = x+2
plus_tw = x -> x+2
#Dont use anonymous functions! But inside other functions it can be practical
f(x,y,z) = (x^2 + 2y)*z


using QuadGK
function integral_of_f(y,z)
    arg(x) = f(x,y,z)
    result = quadgk(arg, 3, 4)
    return result
end

#Positional or keyword arguments are also possible!

a = [1,2,3,4,5]
b = [1.2, 3,4,5]
c = ["Hello", "it's me", "TechyTok"]

#Please not that Julia stores values in memory differently from Python: in Julia to obtain fast loops we need to iter first over columns (which means that the first index must vary first and so on). For this reason if we plan to store, for example, 42 2x2 matrices, we need to create an array of size 2x2x42 (while in Python we would have created a 42x2x2 table).
#Reshape and slicing is possible, also list comprehensions

a=[1,2,3]
b=a
b[2] = 42
print(a)
#similar to python, uses pointers and does not copy!
#In that case use b = copy(a)

tuple1 = (1, 2, 3)
a, b, c = tuple1
#print("$a $b $c") useful way of printing elements
#also possible to return multiple arguments from a function
#also splatting is possible, tuple instead of two arguments!!
#named tuples also possible

person1 = Dict("Name" => "Aurelio", "Phone" => 123456789, "Shoe-size" => 40)
person2 = Dict("Name" => "Elena", "Phone" => 123456789, "Shoe-size" => 36)
addressBook = Dict("Aurelio" => person1, "Elena" => person2)
name1 = "traveler"
name2 = "techytok"
print("Welcome $name1, this is $name2 :)")
#if elseif and else are the ifelse constructions
#also for loops are similar, for i in 1:10, 
#equivalent to               for i = 1:10, 
#loop also over iterables
#while(x<41), enumerate(x) is handy
#for ranges 1:10 works in for but for arrays you have to write collect(1:10)
#In julia for loops are ok! No need to vectorize
#Therefore also no vectorized sin or multiplication of vectors, only matrices, well defined mathematical
#But we can use broadcasting!!!

a = [1,2,3]
sin.(a)
#also parallellism becomes easier!
#let construction for local variables
#constants can be defined never change
#modules are a separate global scope

module ScopeTestModule
export a1
a1 = 25
b1 = 42
end # end of module

using .ScopeTestModule

using SpecialFunctions: gamma, sinint

#import better than using to avoid confusion, but then you have to call the . function
#module does not need indentation but should end with end
#use include for other file modules!